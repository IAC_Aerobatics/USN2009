<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet brings various markup supporting stylesheets together to make-up
the site.  Invoke this one against site markup.  It provides a basic
html document structure.  Use the layout stylesheet to customize the head
content and any internal structure of the body.
-->
<xsl:stylesheet version='1.0' xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="generics.xsl"/>
<xsl:import href="lineage.xsl"/>
<xsl:import href="album.xsl"/>
<xsl:import href="articleList.xsl"/>
<xsl:import href="photostory.xsl"/>
<xsl:import href="schedule.xsl"/>
<xsl:import href="results.xsl"/>
<xsl:import href="layout.xsl"/>

<xsl:output 
 method="html" 
 doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
 doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>

<xsl:template match="/">
<html>
<head>
  <xsl:if test="/node()[@style!='']">
    <link rel="stylesheet" type="text/css">
      <xsl:attribute name="href">
        <xsl:value-of select="/node()/@style"/>
      </xsl:attribute>
    </link>
  </xsl:if>
  <xsl:if test="/node()[@title!='']">
    <title>
      <xsl:value-of select="/node()/@title"/>
    </title>
  </xsl:if>
  <xsl:call-template name="head-content"/>
  <!-- the following enables the album stylesheet to insert JavaScript -->
  <xsl:apply-templates mode="head"/>
</head>
<body> 
  <xsl:call-template name="body-attributes"/>
  <xsl:call-template name="body-content"/>
</body>
</html>
</xsl:template>

<xsl:template match="text()" mode="head"/>

</xsl:stylesheet>
