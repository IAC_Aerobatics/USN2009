<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version='1.0' xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template name="menu-template">
<div class="menu">
    <a class="menu" href="home.html">Home</a>
    <a class="menu" href="sponsors.html">Sponsors</a>
    <a class="menu" href="contact.html">Officials</a>
    <a class="menu" href="schedule.html">Schedule</a>
    <a class="menu" href="reginfo.html">Registration</a>
    <a class="menu" href="accom.html">Location</a>
    <a class="menu" href="past.html">Prior Contests</a>
    <a class="menu" href="cc/index.html">News</a>
    <a class="menu" href="scores.html">Results</a>
    <a class="menu" href="/USNPhotos/usn2009/index.html">Pictures</a>
</div>
</xsl:template>

</xsl:stylesheet>
