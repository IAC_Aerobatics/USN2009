<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet for results.  Supports markup of the form:

<results category="Unlimited" resultBase="results"
  sequenceBase="sequences" overall="Unlimited.htm">
<flight date="Saturday, September 22" name="First Unknown" resultFile="UnlimitedUnknown1.htm">
  <form sel="A" sequenceFile="UnlimitedUnknown1A.jpg"/>
  <form sel="B" sequenceFile="UnlimitedUnknown1B.jpg"/>
</flight>
</results>

The date attribute on the flight element is optional.

Output text file as follows:

overall;Unlimited;results;Unlimited.htm
result;Unlimited;Saturday, September 22;FirstUnkown;results;UnlimitedUnknown1.htm
sequence;Unlimited;sequences;UnlimitedUnknown1A.jpg
sequence;Unlimited;sequences;UnlimitedUnknown1B.jpg

-->

<xsl:stylesheet version='1.0' xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text"/>

<xsl:template match="results">
  <xsl:text>overall;</xsl:text>
  <xsl:value-of select="@category"/>
  <xsl:text>;</xsl:text>
  <xsl:value-of select="@resultBase"/>
  <xsl:text>;</xsl:text>
  <xsl:value-of select="@overall"/>
  <xsl:text>
</xsl:text>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="results/flight">
  <xsl:text>result;</xsl:text>
  <xsl:value-of select="ancestor::results/@category"/>
  <xsl:text>;</xsl:text>
  <xsl:value-of select="@date"/>
  <xsl:text>;</xsl:text>
  <xsl:value-of select="@name"/>
  <xsl:text>;</xsl:text>
  <xsl:value-of select="ancestor::results/@resultBase"/>
  <xsl:text>;</xsl:text>
  <xsl:value-of select="@resultFile"/>
  <xsl:text>
</xsl:text>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="results/flight/form">
  <xsl:text>sequence;</xsl:text>
  <xsl:value-of select="ancestor::results/@category"/>
  <xsl:text>;</xsl:text>
  <xsl:value-of select="ancestor::results/@sequenceBase"/>
  <xsl:text>;</xsl:text>
  <xsl:value-of select="@sequenceFile"/>
  <xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="text()"/>

</xsl:stylesheet>
