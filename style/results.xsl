<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet for results.  Supports markup of the form:

<results category="Unlimited" resultBase="results"
  sequenceBase="sequences">
  <result resultFile="Unlimited.htm"/>
<flight date="Saturday, September 22" name="First Unknown">
  <result resultFile="UnlimitedUnknown1.htm"/>
  <form sel="A" sequenceFile="UnlimitedUnknown1A.jpg"/>
  <form sel="B" sequenceFile="UnlimitedUnknown1B.jpg"/>
</flight>
</results>

The date attribute on the flight element is optional.

Result markup is of form:

<div class="categoryResult">
<div class="categoryResultName">Unlimited</div>
<div class="overallResult">
  <a href="results/Unlimited.htm">Overall Results</a>
</div>
<div class="flightResult">First Unknown. Saturday, September 22
  <div class="resultDataRef">
    <a href="results/UnlimitedUnknown1.htm">results</a>
  </div>
  <div class="sequenceDiagram">
    <a href="sequences/UnlimitedA.jpg">Form A</a>
  </div>
  <div class="sequenceDiagram">
    <a href="sequences/UnlimitedB.jpg">Form B</a>
  </div>
</div>
-->

<xsl:stylesheet version='1.0' xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="results">
<div class="categoryResult">
  <div class="categoryResultName">
    <xsl:value-of select="@category"/>
  </div>
  <xsl:apply-templates/>
</div>
</xsl:template>

<xsl:template match="results/flight">
<div class="result">
  <xsl:value-of select="@name"/>
  <xsl:if test="@date">
    <xsl:text>, </xsl:text>
    <xsl:value-of select="@date"/>
  </xsl:if>
  <xsl:apply-templates/>
</div>
</xsl:template>

<xsl:template name="resultLink">
  <xsl:param name="description"/>
  <div class="resultDataRef">
    <a> 
      <xsl:attribute name="href">
        <xsl:value-of select="ancestor::results/@resultBase"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="@resultFile"/>
      </xsl:attribute>
      <xsl:text><xsl:value-of select="$description"/></xsl:text>
    </a>
  </div>
</xsl:template>

<xsl:template match="flight/result">
  <xsl:call-template name="resultLink">
  <xsl:with-param name="description">Results</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="results/result">
  <xsl:call-template name="resultLink">
  <xsl:with-param name="description">Overall Results</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="results/flight/form">
  <div class="sequenceDiagram">
    <a> 
      <xsl:attribute name="href">
        <xsl:value-of select="ancestor::results/@sequenceBase"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="@sequenceFile"/>
      </xsl:attribute>
      <xsl:text>Form </xsl:text>
      <xsl:value-of select="@sel"/>
    </a>
  </div>
</xsl:template>

</xsl:stylesheet>
