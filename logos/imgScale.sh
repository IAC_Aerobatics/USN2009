#!/bin/bash
#create scaled versions of images in current directory listed on command line
if [ -z "$1" ]
then
echo 'specify images'
else
echo converting in "$(pwd)"
[ -e 100 ] || mkdir 100
[ -e 400 ] || mkdir 400
if [ -d 100 -a -d 400 ]; then
for f in $*
do
  [ "$f" -nt "100/$f" ] && echo "100/$f"
  [ "$f" -nt "100/$f" ] && convert $f -resize x100 "100/$f"
  [ "$f" -nt "200/$f" ] && echo "400/$f"
  [ "$f" -nt "200/$f" ] && convert $f -resize 400x200 "400/$f"
done
else
echo 'failed to create scaled image directory. file in the way?'
fi
fi
