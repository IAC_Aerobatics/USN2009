BEGIN	{ FS=";"; }
/^overall/	{ print "cat overall.html | sed -e '1,$s/{category}/" $2 "/g' >" $4; }
/^result/	{ print "cat flight.html | sed -e '1,$s/{category}/" $2 "/g' |";
              print " sed -e '1,$s/{flight}/" $4 "/g' |"; 
              print " sed -e '1,$s/{schedule}/" $3 "/g' >" $6; }